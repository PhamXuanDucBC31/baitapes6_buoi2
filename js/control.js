export let glassesControl = {
  renderGlasses: (dataGlasses) => {
    let contentHTML = "";
    for (let index = 0; index < dataGlasses.length; index++) {
      let glass = dataGlasses[index];
      let contentIMG = `<img class="col-4" onclick="addGlassToModel(${index})" src="${glass.src}" alt="" />`;
      contentHTML += contentIMG;
      document.getElementById("vglassesList").innerHTML = contentHTML;
    }
  },

  addGlass: (glassID, dataGlasses) => {
    document.getElementById(
      "avatar"
    ).innerHTML = `<img src="${dataGlasses[glassID].virtualImg}" alt="" />`;
  },

  showGlassesInfo: (glassID, dataGlasses) => {
    document.getElementById("glassesInfo").style.display = "block";
    document.getElementById(
      "glassesInfo"
    ).innerHTML = `<div>${dataGlasses[glassID].name} - ${dataGlasses[glassID].brand} (${dataGlasses[glassID].color})</div>
                    <div class="mt-1">
                      <button class="btn btn-danger">$${dataGlasses[glassID].price}</button>
                      <span class="text-success font-weight-bold">${dataGlasses[glassID].stats}</span>
                    </div>
                    <p class="mt-1">${dataGlasses[glassID].description}</p>`;
  },
};

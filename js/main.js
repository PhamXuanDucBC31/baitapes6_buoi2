import { glassesControl } from "./control.js";
import { dataGlasses } from "./model.js";

glassesControl.renderGlasses(dataGlasses);

let addGlassToModel = (id) => {
  glassesControl.addGlass(id, dataGlasses);
  glassesControl.showGlassesInfo(id, dataGlasses);

  let removeGlasses = (status) => {
    if (status === false) {
      document.getElementById("avatar").innerHTML = "";
    } else {
      glassesControl.addGlass(id, dataGlasses);
    }
  };
  window.removeGlasses = removeGlasses;
};
window.addGlassToModel = addGlassToModel;
